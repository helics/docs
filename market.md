**Mercado Brasileiro**

|Empresa   |Produto   |Valor   |Mercado   | Easy-To-Use  | Trial | Suporte |
|---|---|---|---|---|
| nasajon  |http://www.nasajon.com.br/lp-avaliacaodedesempenho?utm_source=google&utm_medium=cpc&utm_term=Software%20Avalia%C3%A7%C3%A3o%20de%20Desempenho&utm_campaign=Avalia%C3%A7%C3%A3o%20de%20Desempenho&gclid=CjwKEAjwxeq9BRDDh4_MheOnvAESJABZ4VTqV6qJK11pfpx9y3LdIYUQR-is7AnY6ceAZy-kNbKiHhoC1pLw_wcB   |  N/A | N/A  |Não|Sim|Sim|
| menviesoftware | https://www.softwareavaliacao.com.br/ |N/A|Mais de 200 empresas|Não|Sim|Sim
| LinkedRH | http://www.joinrh.com.br/avaliacao-de-desempenho-e-competencia/|N/A|http://www.joinrh.com.br/clientes/ | Não(Requer agendamento)|Sim|Sim
| RH1000 | http://www.softwaregestaodepessoas.com.br/sistema.html | N/A | http://www.softwaregestaodepessoas.com.br/clientes.html | Não (Requer contato)| Sim| Sim
| QualyTeam| http://www.qualyteam.com.br/novosite/people.aspx?origem=adw-people&gclid=CjwKEAjwxeq9BRDDh4_MheOnvAESJABZ4VTqooFL_W2Rdp-9w_kWEeLH4YLA36G0rdmcaergHrFiPxoC3xXw_wcB|N/A|N/A|Não (Requer contato)|Sim|Sim

**Mercado Internacional**

http://www.capterra.com/performance-appraisal-software/
