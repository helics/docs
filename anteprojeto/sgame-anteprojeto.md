**FACULDADE DE EDUCAÇÃO TECNOLÓGICA DO ESTADO DO RIO DE JANEIRO –
FAETERJ-Rio**

**SGAME**

**(Sistema Gamificado de Avaliação Multivisão Eletrônico )**

**Genezys Marques Gonçalves**

**Matheus de Moraes Garcia**

Rio de Janeiro – RJ

2016

**SGAME**

**(Sistema Gamificado de Avaliação Multivisão Eletrônico)**

> **Anteprojeto de Trabalho de Conclusão de Curso como requisito básico
> para a apresentação do Trabalho de Conclusão do Curso Superior de
> Tecnologia em Análise de Sistemas Informatizados.**
>
> **Orientadores: **
>
> **MSc. Leonardo Soares Vianna**

Rio de Janeiro - RJ

2016

**SUMÁRIO**

**1. INTRODUÇÃO** 4

**2. JUSTIFICATIVA** 5

**3. OBJETIVOS** 6

> 3.1 GERAL 6
>
> 3.2 ESPECÍFICOS 6

**4. METODOLOGIA** 7

**5. CRONOGRAMA** 8

**REFERÊNCIAS** 9

**1. INTRODUÇÃO**

A produtividade é um dos fatores primordiais para avaliar o crescimento
de uma empresa havendo três linhas de pensamento que se destacam sobre o
tema conforme Moreira (1988): O enfoque na engenharia, na economia, e
nos recursos humanos. Para a concepção do SGAME, consideraremos
principalmente os recursos humanos e suas relações

**2. JUSTIFICATIVA**

A motivação é um dos

**3. OBJETIVOS**

> 3.1 GERAL
>
> 3.2 ESPECÍFICOS

1 – Produzir uma plataforma colaborativa de avaliações acadêmicas por
meio de questões balanceadas de acordo com os pesos atribuídos às
disciplinas;

2 – Gerar provas automáticas e personalizadas de acordo com as
disciplinas ou elementos da ementa concluídos por um discente,
promovendo a interdisciplinaridade das avaliações;

3 – Gerar relatórios de acompanhamento detalhados dos resultados obtidos
pelos discentes para auxílio na tomada de decisões futuras.

**4. METODOLOGIA**

i.  Pesquisa exploratória de ferramentas e técnicas a serem utilizadas
    no projeto;

ii. Pesquisa bibliográfica para levantamento de informações relevantes
    para o projeto.

![](media/image1.wmf){width="5.998611111111111in"
height="2.870833333333333in"}**. CRONOGRAMA**

**REFERÊNCIAS**

ABNT – Associação Brasileira de Normas Técnicas. **NBR 14724**:
Informação e documentação. Trabalhos Acadêmicos - Apresentação**.** Rio
de Janeiro: ABNT,

2002.

SEVERINO, Antonio Joaquim. **Metodologia do trabalho científico.** 22.
ed. rev. e ampl. São Paulo: Cortez, 2002.

POPPER, K.R. ***Conhecimento objetivo**.* São Paulo: EDUSP, 1975.

ETEC TAUBATÉ, MODELO PRÉ-PROJETO TCC. Disponível em:

&lt;http://etectaubate.com.br/wp-content/uploads/2011/09/Modelo-pre-projeto-1.doc&gt;
Acesso em 1º de dezembro de 2014

INEP, Avaliação das Instituições de Nível Superior. Disponível em:

&lt; http://portal.inep.gov.br/superior-avaliacao\_institucional&gt;
Acesso em 5 de novembro de 2014

MEC, Sistema Nacional de Avaliação da Educação Superior. Disponível em:

&lt;
http://portal.mec.gov.br/index.php/?id=12303&option=com\_content&gt;
Acesso em 10 de novembro de 2014.
